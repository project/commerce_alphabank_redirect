Redirection payment for Drupal Commerce and Alphabank (Greece).
By using redirection payments your clients get redirect to the bank and fill in
their credit card data there. No information is stored at the Drupal side.
This ensures security and it costs less because there is no need for
certificates on your server.

This module depends on
http://drupal.org/project/commerce_static_checkout_url
in order to work correctly.
All it does is add functionality specific to Alphabank to the above module.

Upon installation check out admin/commerce/config/static_checkout_url
for the links you should provide to the bank for proper communication
with your Commerce site.

Development
This module is developed by 1024.gr and SRM.gr
Version 2.x of the module is sponsored by Sardine Digital Media.
The developers can create modules for other banks on demand.
This can be done for any bank that uses the redirection methods.
