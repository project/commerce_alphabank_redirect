# Mock server AlphaBank

This module is a simplistic form that enables to test the responses from the AlphaBank endpoint.
This is build upon the implementation of
[commerce_alphabank_redirect](http://drupal.org/project/commerce_alphabank_redirect) so it might
not work outside and is meant only for tests.

## How it works
The only thing needed is to switch the service url in the AlphaBank payment method to not point to
AlphaBank but to `<base url of your website>/mock-server-alphabank`.

The module offers a form where the user can simply select if the transaction is successful, failed or cancelled.
By clicking one of the corresponding data, the user is then redirected to the result page of the commerce corresponding
module.

## Disclaimer
The module does not check for the validity of the data nor does it guarantee to return alphanumeric strings that are
according to the official (unknown) algorithms of the bank.
The module does not check the validity of the digest either as this is being calculated automatically by the
`commerce_alphabank_redirect` module.
